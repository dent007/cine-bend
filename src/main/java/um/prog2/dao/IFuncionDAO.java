package um.prog2.dao;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import um.prog2.model.Funcion;

@Repository
public interface IFuncionDAO extends JpaRepository <Funcion, Integer>{

}
