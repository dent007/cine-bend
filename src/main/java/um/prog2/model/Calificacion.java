package um.prog2.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="calificacion")
public class Calificacion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int calificacion_id;
	
	@Column(name="descripcion",nullable=false,length=200 )
	/*https://www.fotogramas.es/noticias-cine/g23351821/criterios-calificacion-edades-peliculas/ y  https://www.padresfrikis.com/calificacion-por-edades-de-las-peliculas-mas-miticas/ */
	private String descripcion; //Mayores,
	//infantiles "nemo" ,ATP "ET",No < 7 años "harryPoter",No <12 años "enbarazada"
	//No <16 años "mamá" ,No recomendado pa <18 años "700",pelicula X//
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime created;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime update;
	
	public Calificacion() {
		super();
	}
	
	public int getCalificacion_id() {
		return calificacion_id;
	}
	public void setCalificacion_id(int calificacion_id) {
		this.calificacion_id = calificacion_id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getUpdate() {
		return update;
	}

	public void setUpdate(LocalDateTime update) {
		this.update = update;
	}	
}
/*sin anotación para fecha, tiene este formato:
         	"update": {
            "hour": 5,
            "minute": 30,
            "second": 45,
            "nano": 0,
            "monthValue": 11,
            "year": 1989,
            "month": "NOVEMBER",
            "dayOfMonth": 11,
            "dayOfWeek": "SATURDAY",
            "dayOfYear": 315,
            "chronology": {
                "calendarType": "iso8601",
                "id": "ISO"
            }
            Luego de agregar la anotación:
        "update": "1989-11-11T05:30:45"
    }
*/
