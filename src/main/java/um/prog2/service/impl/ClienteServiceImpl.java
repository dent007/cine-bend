package um.prog2.service.impl;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import um.prog2.dao.IClienteDAO;
import um.prog2.dao.IUsuarioDAO;
import um.prog2.dto.UserClienteDto;
import um.prog2.model.Cliente;
import um.prog2.model.Usuario;
import um.prog2.service.IClienteService;
@Service
@Transactional
public class ClienteServiceImpl implements IClienteService{
	
	@Autowired
	private IClienteDAO clientedao;
	
	@Autowired
	private IUsuarioDAO usuariodao;
	
	
	@Override
	/*public int registrar(Usuario usu) {*/
	public int registrar(Cliente cli) {
		int reto=0;	
		System.out.println(cli.toString());		
			//usu.getCliente().setUsuario(usu);
		 //reto=usuariodao.save(usu)!=null?usu.getUsuario_id():0;
		reto=clientedao.save(cli)!=null?cli.getCliente_id():0;
		return reto > 0 ? 1 :0;
	}

	@Override
	public int modificar(Cliente cli) {		
		System.out.println(cli.toString());
		int retorno=0;
		//usuariodao.save(cli.getUsuario());
		retorno=clientedao.save(cli)!=null?cli.getCliente_id():0;
		
		return retorno > 0 ? 1 :0;
	}

	@Override
	public void eliminar(int idCli) {
		clientedao.delete(idCli);
	}

	@Override
	public Cliente listarId(int idCli) {
		return clientedao.findOne(idCli);
	}

	@Override
	public List<Cliente> listar() {
		return clientedao.findAll();
	}

	@Override
	public Page<Cliente> listAllbyPage(Pageable pageable) {
		// TODO Auto-generated method stub
		return clientedao.findAll(pageable);
	}

	@Override
	public int clientOnline(UserClienteDto userLoginDto) {
		UserClienteDto uLDTO= new UserClienteDto();
		uLDTO=userLoginDto;
		//uLDTO.setUserLoing(userLoginDto.getUserLoing());
		int retorno =0;
		Cliente c = new Cliente();
		// TODO Auto-generated method stub
		 c=clientedao.clientOnline(uLDTO.getUserLoing(),uLDTO.getRolLoin());
		  retorno = c.getCliente_id();
		 return retorno;
	}

	@Override
	public Cliente buscarClienteUsuario(UserClienteDto userLoginDto) {
		UserClienteDto uLDTO= new UserClienteDto();
		uLDTO=userLoginDto;

		return  clientedao.findByUsuarioAuthorityAndUsuarioUsername(uLDTO.getUserLoing(),uLDTO.getRolLoin());
		 
	}
}
