package um.prog2.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="ticket")
public class Ticket {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ticket_id;
	
	@ManyToOne
	@JoinColumn(name="cliente_id",nullable=false)
	private Cliente cliente;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime fecha_transaccion;

	@Column(name="butacas", nullable=false)
	private int butacas;
	
	@Column(name="importe", nullable=false)
	private int importe;
	
	@Column(name="pago_uuid", nullable=false)
	private String pago_uuid;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime created;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime update;

	public Ticket() {
		super();
	}

	public int getTicket_id() {
		return ticket_id;
	}

	public void setTicket_id(int ticket_id) {
		this.ticket_id = ticket_id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public LocalDateTime getFecha_transaccion() {
		return fecha_transaccion;
	}

	public void setFecha_transaccion(LocalDateTime fecha_transaccion) {
		this.fecha_transaccion = fecha_transaccion;
	}

	public int getButacas() {
		return butacas;
	}

	public void setButacas(int butacas) {
		this.butacas = butacas;
	}

	public int getImporte() {
		return importe;
	}

	public void setImporte(int importe) {
		this.importe = importe;
	}



	public String getPago_uuid() {
		return pago_uuid;
	}

	public void setPago_uuid(String pago_uuid) {
		this.pago_uuid = pago_uuid;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getUpdate() {
		return update;
	}

	public void setUpdate(LocalDateTime update) {
		this.update = update;
	}
	
}
