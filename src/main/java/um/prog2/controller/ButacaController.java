package um.prog2.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.prog2.model.Butaca;
import um.prog2.service.IButacaService;

@RestController // Esteriotipa la clase como un controlador
@RequestMapping("/butaca") // Indica una ruta de acceso url
public class ButacaController {
	
	@Autowired
	private IButacaService butacaService;
	
	@GetMapping( value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Butaca>> listarButacasTodos() {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		List<Butaca> butaca = new ArrayList<>();
		try {
			butaca=butacaService.listar();
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<List<Butaca>>(butaca, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<List<Butaca>>(butaca, HttpStatus.OK);
	}
	
	@GetMapping( value = "/listar/{idBut}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Butaca> listarButacaPorId(@PathVariable("idBut") Integer idBut) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		Butaca butaca = new Butaca();
		try {
			butaca=butacaService.listarId(idBut);
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Butaca>(butaca, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Butaca>(butaca, HttpStatus.OK);
	}
	
	@DeleteMapping( value = "/eliminar/{idBut}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminarButacaPorId(@PathVariable("idBut") Integer idBut) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Calificacion calificacions = new Calificacion();
		int resultado=0;
		try {
			//calificacions=califServicio.eliminar(idCal);
			butacaService.eliminar(idBut);
			resultado =1;
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			//return new ResponseEntity<Calificacion>(calificacions, HttpStatus.INTERNAL_SERVER_ERROR);
			resultado=0;
		}	
		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}
	@PostMapping( value = "/registrar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrarButaca(@RequestBody Butaca buta) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Butaca butaca = new Butaca(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno=0;
		try {
			retorno=butacaService.registrar(buta); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
	
	@PutMapping( value = "/actualizar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> modificarButaca(@RequestBody Butaca buta) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Butaca butaca = new Butaca(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno=0;
		try {
			retorno=butacaService.modificar(buta); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
}
