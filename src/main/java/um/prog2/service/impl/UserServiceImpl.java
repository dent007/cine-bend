package um.prog2.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import um.prog2.dao.IUsuarioDAO;


/*Gestión de usuarios y sus roles*/
@Transactional

@Service("userDetailsService") //Alias
public class UserServiceImpl implements UserDetailsService{
	
	@Autowired
	private IUsuarioDAO userDAO;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userDAO.findOneByUsername(username);
	}
	
}