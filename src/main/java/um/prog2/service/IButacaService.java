package um.prog2.service;

import java.util.List;

import um.prog2.model.Butaca;

public interface IButacaService {
	/*Retorna el id del últimmo objeto registrado*/
	int registrar(Butaca but);
	int modificar(Butaca but);
	void eliminar(int idBut);
	Butaca listarId(int idBut) ;
	List<Butaca> listar(); 
}
