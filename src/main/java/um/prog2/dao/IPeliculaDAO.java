package um.prog2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import um.prog2.model.Pelicula;

@Repository
public interface IPeliculaDAO extends JpaRepository <Pelicula, Integer>{

}
