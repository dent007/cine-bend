package um.prog2.service;

import java.util.List;

import um.prog2.model.Ticket;

public interface ITicketService {
	int registrar(Ticket tik);
	int modificar(Ticket tik);
	void eliminar(int idTik);
	Ticket listarId(int idTik) ;
	List<Ticket> listar(); 
}
