package um.prog2.service.impl;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import um.prog2.dao.IClienteDAO;
import um.prog2.dao.IPagoDAO;
import um.prog2.dao.ITarjetaDAO;
import um.prog2.dto.DisponibilidadTarjetaPagoDTO;
import um.prog2.dto.PagoClienteDTO;
import um.prog2.dto.TarjetaSaldoDTO;
import um.prog2.model.Cliente;
import um.prog2.model.Pago;
import um.prog2.model.Tarjeta;
import um.prog2.model.Tarjeta.Tipo;
import um.prog2.service.ITarjetaService;

@Service
@Transactional
public class TarjetaServicioImpl implements ITarjetaService{

	@Autowired
	private ITarjetaDAO tarjetaDao;
	
	@Autowired
	private IClienteDAO clientDao;

	@Autowired
	private IPagoDAO pagoDao;

	@Override
	public int registrar(Tarjeta tar) {
		int salida=0;
		salida = tarjetaDao.save(tar) !=null ? tar.getTarjeta_id():0;
		return salida > 0 ? 1 : 0;
	}

	@Override
	public int modificar(Tarjeta tar) {
		int salida=0; 
		salida = tarjetaDao.save(tar) !=null ? tar.getTarjeta_id():0;
		return salida > 0 ? 1 : 0;
	}

	@Override
	public void eliminar(int idTar) {
		tarjetaDao.delete(idTar);
		
	}

	@Override
	public Tarjeta listarId(int idTar) {
		
		return tarjetaDao.findOne(idTar);
	}

	@Override
	public List<Tarjeta> listar() {
		
		//System.out.println("ssss" + tarjetaDao.findAll().size());
		return tarjetaDao.findAll(); 
	}
	 //pagoClienteDTO{int tarjeta_id,int valorEntrada, String uuid}

	@Override
	public int pagarActalizarSaldo(PagoClienteDTO pagoClienteDTO) {		
		int estadoInsertarPago=0;
		 //1° Buscar tarjeta y actualizar el saldo
		Tarjeta tarjetaCambiada = new Tarjeta();
		
		tarjetaCambiada= tarjetaDao.findOne(pagoClienteDTO.getTarjeta_id());		
		int nuevoSaldo=( tarjetaCambiada.getSaldo() )-( pagoClienteDTO.getValorEntrada() );
		tarjetaCambiada.setSaldo(nuevoSaldo);
		
		// 2° Registrando el pago y la tarjeta modificada
		Pago pagando= new Pago();
		pagando.setTarjeta(tarjetaDao.save(tarjetaCambiada));
		pagando.setImporte(pagoClienteDTO.getValorEntrada());
		pagando.setPago_uuid(pagoClienteDTO.getUuid());
		pagando.setCreated(LocalDateTime.now());
		pagando.setUpdated(LocalDateTime.now());
		//	salida = tarjetaDao.save(tar) !=null ? tar.getTarjeta_id():0;
		//return salida > 0 ? 1 : 0;
		estadoInsertarPago = pagoDao.save(pagando) !=null? pagando.getPago_id():0;		
		return estadoInsertarPago;
	}

	
	@Override
	// Recibe=> PagoClienteDTO={int clienteId, String tarjetaNumero,private int valorEntrada,private Tipo tipo;}
	public TarjetaSaldoDTO preguntarSaldoTarjeta(DisponibilidadTarjetaPagoDTO disponibilidadTarjetaPagoDTO) {
		
		
		Cliente cliente = new Cliente();
		cliente.setCliente_id(disponibilidadTarjetaPagoDTO.getClienteId());
		System.out.println("DOS "+disponibilidadTarjetaPagoDTO.toString());

		int  saldo=-1; 
		int  idTarjeta=-1; 
						
		Tarjeta tarjetaReferenciada;// = new Tarjeta();
		TarjetaSaldoDTO tSaldoDTO= new TarjetaSaldoDTO();
		
		tarjetaReferenciada=tarjetaDao.findByNumeroAndClienteAndTipo(disponibilidadTarjetaPagoDTO.getTarjetaNumero(),cliente,disponibilidadTarjetaPagoDTO.getTipo());
		// SOLO SI LA TARJETA EXISTE ENTRA A ESTE IF, EN TODOS LOS OTROS CASOS ENVIO 0 y -1
		if(tarjetaReferenciada!=null) { // SI ok; tarjetaReferenciada=> "idTarjeta": 4 y  "saldo": 800
				saldo =  (  (tarjetaReferenciada.getSaldo()) - (disponibilidadTarjetaPagoDTO.getValorEntrada())  );
				idTarjeta=tarjetaReferenciada.getTarjeta_id();
				System.out.println("if > "+tarjetaReferenciada.toString());
		}		
				
		// Si no hay tarjetaRefenciada =>  Siempre ["idTarjeta": 0, "saldo": -1]
		tSaldoDTO.setSaldo(saldo);
		tSaldoDTO.setIdTarjeta(idTarjeta);
					
		System.out.println("ww--> "+tSaldoDTO.toString());
		
		return tSaldoDTO;// Retorno=> TarjetaSaldoDTO={int idTarjeta;int saldo}	
	}	 

}
/*Uno PagoClienteDTO [clienteId=1, tarjetaNumero=4444444444444444, valorEntrada=23, tipo=DEBITO]
--> Tarjeta [tarjeta_id=4, numero=4444444444444444, cliente=Cliente [cliente_id=1, usuario=um.prog2.model.Usuario@8e694c, nombre=Nombre1, apellido=Apellido1, documento=123456, created=2019-03-25T13:59:04.087, update=2019-03-25T13:59:04.087], saldo=850, created=2019-03-25T13:59:04.087, update=2019-03-25T13:59:04.087, tipo=DEBITO]
retu0
Uno PagoClienteDTO [clienteId=1, tarjetaNumero=444444444444444, valorEntrada=23, tipo=DEBITO]*/


