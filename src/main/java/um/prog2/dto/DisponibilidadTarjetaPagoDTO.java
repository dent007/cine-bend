package um.prog2.dto;

import um.prog2.model.Tarjeta.Tipo;

public class DisponibilidadTarjetaPagoDTO {
	private int clienteId;
	private String tarjetaNumero;
	private int valorEntrada;
	private Tipo tipo;
	
	
	public DisponibilidadTarjetaPagoDTO() {
		super();
	}
	public int getClienteId() {
		return clienteId;
	}
	public void setClienteId(int clienteId) {
		this.clienteId = clienteId;
	}
	public String getTarjetaNumero() {
		return tarjetaNumero;
	}
	public void setTarjetaNumero(String tarjetaNumero) {
		this.tarjetaNumero = tarjetaNumero;
	}
	public int getValorEntrada() {
		return valorEntrada;
	}
	public void setValorEntrada(int valorEntrada) {
		this.valorEntrada = valorEntrada;
	}
	public Tipo getTipo() {
		return tipo;
	}
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
	@Override
	public String toString() {
		return "DisponibilidadTarjetaPagoDTO [clienteId=" + clienteId + ", tarjetaNumero=" + tarjetaNumero
				+ ", valorEntrada=" + valorEntrada + ", tipo=" + tipo + "]";
	}





}
