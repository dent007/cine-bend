package um.prog2.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="ocupacion")
public class Ocupacion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private int ocupacion_id;
	
	@ManyToOne
	@JoinColumn(name="funcion_id", nullable=false )
	private Funcion funcion;
	
	@ManyToOne
	@JoinColumn(name="butaca_id", nullable=false )
	private Butaca butaca;
	
	@ManyToOne
	@JoinColumn(name="ticket_id", nullable=false )
	private Ticket ticket;
	
	@ManyToOne
	@JoinColumn(name="entrada_id", nullable=false )
	private Entrada entrada;
	
	@Column(name="valor", nullable=false)
	private int valor;
	
	@JsonSerialize(using=ToStringSerializer.class)	
	private LocalDateTime created;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime update;
	
	public Ocupacion() {
		super();
	}
	public int getOcupacion_id() {
		return ocupacion_id;
	}
	public void setOcupacion_id(int ocupacion_id) {
		this.ocupacion_id = ocupacion_id;
	}	
	
	public Butaca getButaca() {
		return butaca;
	}
	public void setButaca(Butaca butaca) {
		this.butaca = butaca;
	}
	
	public Funcion getFuncion() {
		return funcion;
	}
	public void setFuncion(Funcion funcion) {
		this.funcion = funcion;
	}
	public Ticket getTicket() {
		return ticket;
	}
	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}
	public Entrada getEntrada() {
		return entrada;
	}
	public void setEntrada(Entrada entrada) {
		this.entrada = entrada;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public LocalDateTime getCreated() {
		return created;
	}
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
	public LocalDateTime getUpdate() {
		return update;
	}
	public void setUpdate(LocalDateTime update) {
		this.update = update;
	}
	
	
}
