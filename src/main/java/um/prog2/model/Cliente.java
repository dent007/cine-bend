package um.prog2.model;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="cliente")

public class Cliente { //padre de Usuario en @MapsId, no existe hijo (usuario) sin padre!
	
	/*
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int cliente_id;
	
	@JsonIgnore  // o de lo contrario se hace siclico
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "cliente")
	private Usuario usuario;*/
	
	
	//
	
	@Id
	private int cliente_id;
	

	@OneToOne(cascade = CascadeType.ALL)
	@MapsId
	@JoinColumn(name = "cliente_id", nullable = false)
	private Usuario usuario;
	//
	
	
	
	@Column(name="nombre", nullable=false)
	private String nombre;
	
	@Column(name="apellido", nullable=false)
	private String apellido;

	@Column(name="documento", nullable=false)
	private String documento;
	

	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime created;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime update;
	
	
	public Cliente() {
		super();
	}


	public int getCliente_id() {
		return cliente_id;
	}


	public void setCliente_id(int cliente_id) {
		this.cliente_id = cliente_id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getDocumento() {
		return documento;
	}


	public void setDocumento(String documento) {
		this.documento = documento;
	}


	public LocalDateTime getCreated() {
		return created;
	}


	public void setCreated(LocalDateTime created) {
		this.created = created;
	}


	public LocalDateTime getUpdate() {
		return update;
	}


	public void setUpdate(LocalDateTime update) {
		this.update = update;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	@Override
	public String toString() {
		return "Cliente [cliente_id=" + cliente_id + ", usuario=" + usuario + ", nombre=" + nombre + ", apellido="
				+ apellido + ", documento=" + documento + ", created=" + created + ", update=" + update + "]";
	}
	
	
}
