package um.prog2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import um.prog2.model.Entrada;

@Repository
public interface IEntradaDAO extends JpaRepository <Entrada, Integer> {

}
