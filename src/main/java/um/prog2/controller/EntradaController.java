package um.prog2.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.prog2.model.Entrada;
import um.prog2.service.IEntradaServicio;

@RestController 
@RequestMapping("/entrada") 
public class EntradaController {
	
	@Autowired
	private IEntradaServicio entradaservicio;

	
	@GetMapping( value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Entrada>> listarEntradasTodas() {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		List<Entrada> entrada = new ArrayList<>();
		try {
			entrada=entradaservicio.listar();
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<List<Entrada>>(entrada, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<List<Entrada>>(entrada, HttpStatus.OK);
	}
	
	@GetMapping( value = "/listar/{idEnt}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Entrada> listarEntradaPorId(@PathVariable("idEnt") Integer idEnt) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		Entrada entrada = new Entrada();
		try {
			entrada=entradaservicio.listarId(idEnt);
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Entrada>(entrada, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Entrada>(entrada, HttpStatus.OK);
	}
	
	@DeleteMapping( value = "/eliminar/{idEnt}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminarEntradaPorId(@PathVariable("idEnt") Integer idEnt) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Calificacion calificacions = new Calificacion();
		int resultado=0;
		try {
			//calificacions=califServicio.eliminar(idCal);
			entradaservicio.eliminar(idEnt);
			resultado =1;
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			//return new ResponseEntity<Calificacion>(calificacions, HttpStatus.INTERNAL_SERVER_ERROR);
			resultado=0;
		}	
		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}
	
	@PostMapping( value = "/registrar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrarEntrada(@RequestBody Entrada ent) {// @RequestBody formatea a JSON
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Entrada entrada = new Entrada(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno =0;
		try {
			retorno=entradaservicio.registrar(ent); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
	@PutMapping( value = "/actualizar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> modificarEntrada(@RequestBody Entrada ent) {// @RequestBody formatea a JSON
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Entrada entrada = new Entrada(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno =0;
		try {
			retorno=entradaservicio.modificar(ent); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
}
