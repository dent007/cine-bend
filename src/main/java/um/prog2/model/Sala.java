package um.prog2.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
@Entity
@Table(name="sala")
public class Sala {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int sala_id;
	
	@Column(name="descripcion",nullable=false)
	private String descripcion;
	
	@Column(name="capacidad",nullable=false)
	private int capacidad;
	
		
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime created;

	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime update;


	public Sala() {
		super();
	}
		
	public int getSala_id() {
		return sala_id;
	}

	public void setSala_id(int sala_id) {
		this.sala_id = sala_id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getUpdate() {
		return update;
	}

	public void setUpdate(LocalDateTime update) {
		this.update = update;
	}

	
}
